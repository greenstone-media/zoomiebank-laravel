<?php

return [
    'client_key' => env('ZOOMIEBANK_CLIENT_KEY'),
    'base_url' => env('ZOOMIEBANK_API_URL', 'https://zoomiebank.staging.greenstonemedia.com'),
    'account_user' => env('ZOOMIEBANK_USERNAME'),
    'account_pass' => env('ZOOMIEBANK_PASSWORD'),
    'log_calls' => env('ZOOMIEBANK_LOG_CALLS', false)
];
