<?php

namespace GreenstoneMedia\ZoomieBank\Traits;

use Config;
use Log;

trait UserFunctions
{
    public function login(){
        return $this->_apiClient->call('POST','/login',[
            'username' => Config::get('zoomiebank.account_user'),
            'password' => Config::get('zoomiebank.account_pass')
        ]);
    }

    public function refresh($token){
        return $this->_apiClient->call('GET','/refresh',[
            'token' => $this->_apiClient->getUserToken()
        ]);
    }
}