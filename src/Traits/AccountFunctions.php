<?php

namespace GreenstoneMedia\ZoomieBank\Traits;

use Log;

trait AccountFunctions
{
    public function getAccount() 
    {
        return $this->_apiClient->call('GET', '/account', [
            'key' => $this->_apiClient->getClientKey(),
            'token' => $this->_apiClient->getUserToken()
        ]);
    }

    public function getPoints($key, $token){
        return $this->_apiClient->call('GET', '/account/points', [
            'key' => $this->_apiClient->getClientKey(),
            'token' => $this->_apiClient->getUserToken()
        ]);
    }
}