<?php

namespace GreenstoneMedia\ZoomieBank\Traits;

use Log;

trait CardFunctions
{
    public function getCardCatalog(){
        return $this->_apiClient->call('GET', '/account/cards', [
            'key' => $this->_apiClient->getClientKey(),
            'token' => $this->_apiClient->getUserToken()
        ]);
    }
}

