<?php
/**
 * Exception used when there is a client exception during the GuzzleHttp call
 *
 * @category Exception
 * @package  GreenstoneMedia\ZoomieBank\Exceptions
 * @author   Greenstone Media <daniel@greenstonemedia.com>
 * @license  <manheim.com> Proprietary
 * @link     http://www.manheim.com/
 */
namespace GreenstoneMedia\ZoomieBank\Exceptions;

use Exception;
use Log;
/**
 * Exception used when there is a client exception during the GuzzleHttp call
 *
 * @category Exception
 * @package  GreenstoneMedia\ZoomieBank\Exceptions
 * @author   Greenstone Media <daniel@greenstonemedia.com>
 * @license  <manheim.com> Proprietary
 * @link     http://www.manheim.com/
 */
class ZoomieBankDataException extends Exception
{
    /**
     * Constructor for the exception
     *
     * @param string    $messages Message to pass to the parent exception
     * @param integer   $code     Exception code to use
     * @param Exception $previous Previous exception if chained
     */
    public function __construct($messages, $code = 0, Exception $previous = null) 
    {
        // some code
        Log::critical('Client Exception For ZoomieBank Call');
        Log::critical($messages);
        // make sure everything is assigned properly
        parent::__construct('A ZoomieBank Call failed validation', $code, $previous);
    }
}