<?php
/**
 * API Client for interacting with the ZoomieBank API
 *
 * @category Service
 * @package  GreenstoneMedia\ZoomieBank
 * @author   Greenstone Media <daniel@greenstonemedia.com>
 * @license  MIT
 * @link     http://zoomiebank.staging.greenstonemedia.com/
 */
namespace GreenstoneMedia\ZoomieBank\Utilities;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Firebase\JWT\JWT;
use Log;
use Config;
use Debugbar;
/**
 * API Client for interacting with the ZoomieBank API
 *
 * @category Service
 * @package  GreenstoneMedia\ZoomieBank
 * @author   Greenstone Media <daniel@greenstonemedia.com>
 * @license  <zoomiebank.staging.greenstonemedia.com> Proprietary
 * @link     http://zoomiebank.staging.greenstonemedia.com/
 */
class ApiClient
{
    private $_user_token,
            $_client_key,
            $_guzzleClient = null;
    private $_headers = [];
    private $_prepOutput = true;
    public $errors = [];

    /**
     * Constructor for the APIClient class
     *
     * @param string $user_token User token to use when authenticating
     */
    public function __construct($user_token = null, $client_key = null)
    {
        $this->_user_token = $user_token;
        $this->client_key = $client_key;
        $this->init();
    }

    /**
     * Get the API Client's user token
     *
     * @return string
     */
    public function getUserToken(){
        return $this->$_user_token;
    }

    /**
     * Get the API Client's client key
     *
     * @return string
     */
    public function getClientKey(){
        return $this->$_client_key;
    }
    
    /**
     * Initializes the headers for the guzzle client to interact with
     * the API
     *
     * @return array
     */
    private function _initializeHeaders()
    {
        $this->_clearHeaders();

        $this->setHeader('Content-Type', 'application/json');

        if ($this->_user_token !== null) {
            $this->setAuthToken($this->_user_token);
        }

        return $this->_headers;
    }

    /**
     * Initializes the guzzle client and sets default headers
     * needed for every call
     *
     * @return void
     */
    public function init()
    {
        $this->_headers = $this->_initializeHeaders();

        $guzzleConfig = [
            'base_uri' => Config::get('zoomiebank.base_url'),
            'headers'=>$this->_headers
        ];

        $this->_guzzleClient = new GuzzleClient($guzzleConfig);
    }
    /**
     * Returns the current guzzle client the ApiClient is using to
     * make calls
     *
     * @return GuzzleClient
     */
    public function getClient()
    {
        return $this->_guzzleClient;
    }

    /**
     * Sets a header on the guzzle client
     *
     * @param string $key   Header key
     * @param string $value Header value
     * 
     * @return void
     */
    public function setHeader($key,$value)
    {
        $this->_headers[$key] = $value;
    }

    /**
     * Clears the current guzzle client headers
     * 
     * @return void
     */
    private function _clearHeaders()
    {
        $this->_headers = [];
    }

    /**
     * Set the authentication header to the provided token
     *
     * @param string $token Token to use for the authorization header
     * 
     * @return void
     */
    public function setAuthToken($token)
    {
        $this->setHeader(
            'Authorization', sprintf('Bearer %s', $token)
        );
    }

    /**
     * Performs a call using the current guzzle client
     *
     * @param string $method REST Method used for the call
     * @param string $url    URL for the call
     * @param array  $data   Data to be passed to the API
     * 
     * @return \Illuminate\Support\Collection
     */
    public function call($method,$url,$data=[])
    {
        $callBody = [
            'headers' => $this->_headers
        ];

        if ($data !== []) {
            $callBody['json'] = $data;
        }

        try{
            $response = $this->_guzzleClient->request($method, $url, $callBody);
        } catch( ClientException $e ){
            Debugbar::error(
                "ZoomieBank API Call Error : Client",
                $e->getResponse()->getBody()->getContents()
            );
            return $this->output($e->getResponse());
        } catch( ServerException $e ){
            Debugbar::error(
                "ZoomieBank API Call Error : Server",
                $e->getResponse()->getBody()->getContents()
            );
            return $this->output($e->getResponse());
        }

        if ($method == 'POST' 
            && ($url == '/subscriptions' || $url == '/subscribers')
            && isset($response->getHeaders()['Location'])
        ) {
            return $response->getHeaders()['Location'][0];
        }


        if (\App::environment() == 'testing' || Config::get('zoomiebank.log_calls')) {
            Log::info(
                'Called ZoomieBank',
                [
                    'method'=>$method,
                    'base_url'=>Config::get('zoomiebank.base_url'),
                    'url'=>$url,
                    'data'=>$data,
                    'headers'=>$this->_headers,
                    'responseStatusCode'=>$response->getStatusCode(),
                    'response'=>(string)$response->getBody()
                ]
            );
        }

        return $this->output($response);
    }

    /**
     * Prepares the guzzle client response to be output to the
     * laravel application
     *
     * @param \GuzzleHttp\Response $response Response from a guzzle api call
     * 
     * @return String
     */
    public function output($response)
    {
        return $this->_prepOutput ?
            json_decode((string) $response->getBody()) :
            (string) $response->getBody();
    }

    /**
     * Turns output preparation off
     * 
     * @return void
     */
    public function prepOutputOff()
    {
        $this->_prepOutput = false;
    }

    /**
     * Turns output preparation on
     * 
     * @return void
     */
    public function prepOutputOn()
    {
        $this->_prepOutput = true;
    }
}