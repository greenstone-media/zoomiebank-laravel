<?php
/**
 * Entry point into the ZoomieBank API functionality
 *
 * @category Service
 * @package  GreenstoneMedia\ZoomieBank
 * @author   Greenstone Media <daniel@greenstonemedia.com>
 * @license  <manheim.com> Proprietary
 * @link     https://zoomiebank.staging.greenstonemedia.com/
 */
namespace GreenstoneMedia\ZoomieBank;

use Cache;

use GreenstoneMedia\ZoomieBank\ApiClient;
use GreenstoneMedia\ZoomieBank\Traits\AccountFunctions;
use GreenstoneMedia\ZoomieBank\Traits\CardFunctions;
use GreenstoneMedia\ZoomieBank\Traits\UserFunctions;

use Debugbar;
/**
 * Entry point into the ZoomieBank API functionality
 *
 * @category Service
 * @package  GreenstoneMedia\ZoomieBank
 * @author   Greenstone Media <daniel@greenstonemedia.com>
 * @license  MIT
 * @link     https://zoomiebank.staging.greenstonemedia.com/
 */
class ZoomieBankService
{
    use AccountFunctions, CardFunctions, UserFunctions;

    private $_apiClient;
    private $_bearerKey = 'zoomiebank_bearer_token';
    
    /**
     * Constructor for the PiPoints service.
     *
     * @param String $user_token Token to use for authentication (OPTIONAL)
     */
    public function __construct($user_token=null)
    {
        $this->initApiClient($user_token);
    }

    /**
     * Initializes the API client. It accepts an optional user token,
     * and attempts to generate one if none is provided. It also caches the token
     * once generated and uses that going forward after the initial generation of
     * the token
     *
     * @param String|Null $user_token Token to use for authentication (OPTIONAL)
     * 
     * @return void
     */
    public function initApiClient($user_token)
    {
        $clientKey = Config::get('zoomiebank.client_key');

        if(!$clientKey){
            throw new \Exception('No ZoomieBank Client Key Is Set');
        }

        if ($user_token !== null) {
            $this->_apiClient = new ApiClient($user_token, $clientKey);
            Debugbar::info('ZoomieBank SDK : User Token Supplied');
        } else if (Cache::has($this->_bearerKey)) {
            $this->_apiClient = new ApiClient(Cache::get($this->_bearerKey, $clientKey));
            Debugbar::info('ZoomieBank SDK : User Token Supplied From Cache');
        } else {
            $user_token = $this->getUserToken();
            Cache::add($this->_bearerKey, $user_token->token, 24*60);
            $this->_apiClient = new ApiClient($user_token->token, $clientKey);
            Debugbar::info('ZoomieBank SDK : User Token Generated');
        }
    }

    /**
     * Retrieve a user token from the API based on the .env configuration
     *
     * @return \GuzzleHttp\Response
     */
    public function getUserToken()
    {
        $apiClient = new ApiClient();
        return $apiClient->login();
    }

    /**
     * Returns the current error array from the apiClient
     *
     * @return array
     */
    public function getApiErrors()
    {
        return $this->apiClient->errors;
    }
}
